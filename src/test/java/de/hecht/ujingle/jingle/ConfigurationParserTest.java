package de.hecht.ujingle.jingle;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ConfigurationParserTest {

    @Test
    void getJingles() {
        var parser = new ConfigurationParser("src/test/resources/ujingle.config");

        var jingles = parser.getJingles();

        assertThat(jingles).hasSize(1);
    }

}