#!/bin/bash

if [ -z ${UJINGLE_HOME} ]
then
  DIRNAME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
  UJINGLE_HOME="`cd "$DIRNAME/..";pwd`"
fi;

PID=`$UJINGLE_HOME/bin/ujingleps.sh | awk '{print $2}'`

if [ -z "${PID}" ]
then
        echo "Ujingle process is not running. No shutdown will be performed."
        exit 0
fi

echo "Shutting down Ujingle."

kill $PID;

echo "Shutdown done."