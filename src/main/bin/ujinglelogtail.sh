#!/bin/bash

if [ -z ${UJINGLE_HOME} ]
then
  DIRNAME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
  UJINGLE_HOME="`cd "$DIRNAME/..";pwd`"
fi;

if [ -z ${UJINGLE_LOG_PATH} ]
then
  UJINGLE_LOG_PATH=$UJINGLE_HOME/log
fi;

UJINGLE_LOG_FILE=${UJINGLE_LOG_PATH}/ujingle.log

echo "tail -F ${UJINGLE_LOG_FILE}"
echo
exec tail -F ${UJINGLE_LOG_FILE}