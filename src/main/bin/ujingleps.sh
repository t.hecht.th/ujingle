#!/bin/bash

if [ -z ${UJINGLE_HOME} ]
then
  DIRNAME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
  UJINGLE_HOME="`cd "$DIRNAME/..";pwd`"
fi;

ps auxww | grep $UJINGLE_HOME/lib/Ujingle.jar | grep -v grep