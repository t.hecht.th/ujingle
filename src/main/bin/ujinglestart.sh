#!/bin/bash

if [ -z ${JAVA_HOME} ]
then
  JAVA_PATH=`which java`
else
  JAVA_PATH=$JAVA_HOME/bin/java
fi;

if [ -z ${UJINGLE_HOME} ]
then
  DIRNAME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
  UJINGLE_HOME="`cd "$DIRNAME/..";pwd`"
fi;

if [ -z ${UJINGLE_LOG_PATH} ]
then
  UJINGLE_LOG_PATH=$UJINGLE_HOME/log
fi;

$JAVA_PATH --add-opens java.base/java.lang=ALL-UNNAMED -Dloader.main=de.hecht.ujingle.Ujingle -DUJINGLE_LOG_PATH=${UJINGLE_LOG_PATH} -jar $UJINGLE_HOME/lib/Ujingle.jar --config.path=$1 > $UJINGLE_LOG_PATH/ujingle_console.log 2>&1 &

echo "Finished starting Ujingle"
echo `$UJINGLE_HOME/bin/ujingleps.sh`