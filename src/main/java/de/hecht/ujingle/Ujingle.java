package de.hecht.ujingle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Ujingle implements ApplicationListener<ApplicationEvent> {
    private static final Logger logger = LoggerFactory.getLogger(Ujingle.class);

    public static void main(String[] args) {
        SpringApplication.run(Ujingle.class, args);
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if(event instanceof ApplicationReadyEvent) {
            logger.info("Ujingle started.");
        }

        if(event instanceof ContextClosedEvent) {
            logger.info("Closing Ujingle.");
        }
    }
}
