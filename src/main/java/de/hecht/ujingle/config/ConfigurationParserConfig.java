package de.hecht.ujingle.config;

import de.hecht.ujingle.jingle.ConfigurationParser;
import de.hecht.ujingle.jingle.Jingle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class ConfigurationParserConfig {
    private static final Logger logger = LoggerFactory.getLogger(ConfigurationParserConfig.class);

    @Bean
    public ConfigurationParser configurationParser(@Value("${config.path}") String configPath) {
        logger.info("Loading configuration {}", configPath);
        return new ConfigurationParser(configPath);
    }

    @Bean
    public List<Jingle> jingles(ConfigurationParser configurationParser) {
        return configurationParser.getJingles();
    }
}
