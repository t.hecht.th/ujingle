package de.hecht.ujingle.config;

import de.hecht.ujingle.jingle.Jingle;
import de.hecht.ujingle.jingle.JingleJukebox;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;

import java.time.Instant;
import java.util.List;

@Configuration
public class JukeboxConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(JukeboxConfiguration.class);

    private final TaskScheduler taskScheduler;

    private final List<Jingle> jingles;

    private final JingleJukebox jingleJukebox;

    public JukeboxConfiguration(TaskScheduler taskScheduler, List<Jingle> jingles, JingleJukebox jingleJukebox) {
        this.taskScheduler = taskScheduler;
        this.jingles = jingles;
        this.jingleJukebox = jingleJukebox;
    }

    @PostConstruct
    public void scheduleJingles() {
        initMediaPlayerCapability();
        logger.info("Starting scheduler");
        jingles.forEach(this::configureJingle);
    }

    private void configureJingle(Jingle jingle) {
        var now = Instant.now();
        if (now.isAfter(jingle.instant())) {
            if (logger.isWarnEnabled()) {
                logger.warn("Timestamp {} is in the past. Skipping scheduling of jingle {}", jingle.instant(), jingle.filepath());
            }
        } else {
            if (logger.isInfoEnabled()) {
                logger.info("Scheduling {} at {}", jingle.filepath(), jingle.instant());
            }
            taskScheduler.schedule(() -> jingleJukebox.playSound(jingle.filepath()), jingle.instant());
        }
    }

    private static void initMediaPlayerCapability() {
        com.sun.javafx.application.PlatformImpl.startup(() -> {});
    }
}
