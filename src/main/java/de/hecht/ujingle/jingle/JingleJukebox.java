package de.hecht.ujingle.jingle;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class JingleJukebox {
    private static final Logger logger = LoggerFactory.getLogger(JingleJukebox.class);

    private final SpotifyGateway spotifyGateway;

    // Field to hold the reference. Otherwise, the jingle might get interrupted
    private MediaPlayer mediaPlayer;

    public JingleJukebox(SpotifyGateway spotifyGateway) {
        this.spotifyGateway = spotifyGateway;
    }

    public void playSound(String filePath) {
        logger.info("-----");
        logger.info("Starting Jingle {} in jukebox", filePath);
        spotifyGateway.pauseSpotify();
        logger.info("-");
        Media media = new Media(new File(filePath).toURI().toString());
        MediaPlayer player = new MediaPlayer(media);
        player.setOnPlaying(() -> logger.info("Playing file: {}", filePath));
        player.setOnEndOfMedia(() -> {
            this.mediaPlayer = null;
            logger.info("Finished playing file: {}", filePath);
            spotifyGateway.playSpotify();
        });
        this.mediaPlayer = player;
        player.play();
    }
}
