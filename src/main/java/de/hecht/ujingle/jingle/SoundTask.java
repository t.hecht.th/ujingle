package de.hecht.ujingle.jingle;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class SoundTask implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(SoundTask.class);

    private final String filePath;

    public SoundTask(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }

    @Override
    public void run() {
        logger.info("-----");
        logger.info("Processing Jingle event");
        pauseSpotify();
        playSound();
    }

    private void pauseSpotify() {
        logger.info("-");
        logger.info("Sending pause command to spotify");
        try {
            ProcessBuilder pb = new ProcessBuilder();
            pb.command("spotify", "pause");
            Process process = pb.start();

            var reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            reader.lines().forEach(s -> logger.info("Process output: {}", s));

            process.waitFor();
            logger.info("Finished sending pause command to spotify");
        } catch (IOException e) {
            logException(e);
        } catch (InterruptedException e) {
            logException(e);
            Thread.currentThread().interrupt();
        }
    }

    private void playSound() {
        logger.info("-");
        Media media = new Media(new File(filePath).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setOnPlaying(() -> logger.info("Playing file: {}", filePath));
        mediaPlayer.setOnEndOfMedia(() -> {
            logger.info("Finished playing file: {}", filePath);
            this.playSpotify();
        });
        mediaPlayer.play();
    }

    private void playSpotify() {
        logger.info("-");
        logger.info("Sending play command to spotify");
        try {
            ProcessBuilder pb = new ProcessBuilder();
            pb.command("spotify", "play");
            Process process = pb.start();

            var reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            reader.lines().forEach(s -> logger.info("Process output: {}", s));

            process.waitFor();
            logger.info("Finished sending play command to spotify");
        } catch (IOException e) {
            logException(e);
        } catch (InterruptedException e) {
            logException(e);
            Thread.currentThread().interrupt();
        }
        logger.info("-");
    }

    private static void logException(Exception e) {
        logger.error("Failed to send command to spotify", e);
    }
}
