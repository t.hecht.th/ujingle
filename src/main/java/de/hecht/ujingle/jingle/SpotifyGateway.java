package de.hecht.ujingle.jingle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
public class SpotifyGateway {
    private static final Logger logger = LoggerFactory.getLogger(SpotifyGateway.class);

    public void pauseSpotify() {
        logger.info("-");
        logger.info("Sending pause command to spotify");
        try {
            ProcessBuilder pb = new ProcessBuilder();
            pb.command("spotify", "pause");
            Process process = pb.start();

            var reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            reader.lines().forEach(s -> logger.info("Process output: {}", s));

            process.waitFor();
            logger.info("Finished sending pause command to spotify");
        } catch (IOException e) {
            logException(e);
        } catch (InterruptedException e) {
            logException(e);
            Thread.currentThread().interrupt();
        }
    }

    public void playSpotify() {
        logger.info("-");
        logger.info("Sending play command to spotify");
        try {
            ProcessBuilder pb = new ProcessBuilder();
            pb.command("spotify", "play");
            Process process = pb.start();

            var reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            reader.lines().forEach(s -> logger.info("Process output: {}", s));

            process.waitFor();
            logger.info("Finished sending play command to spotify");
        } catch (IOException e) {
            logException(e);
        } catch (InterruptedException e) {
            logException(e);
            Thread.currentThread().interrupt();
        }
        logger.info("-");
    }

    private static void logException(Exception e) {
        logger.error("Failed to send command to spotify", e);
    }
}
