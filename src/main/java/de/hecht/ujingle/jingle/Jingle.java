package de.hecht.ujingle.jingle;

import java.time.Instant;

public record Jingle(String filepath, Instant instant) { }
