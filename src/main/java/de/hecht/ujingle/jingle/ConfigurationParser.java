package de.hecht.ujingle.jingle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Collections;
import java.util.List;

public class ConfigurationParser {
    private static final Logger logger = LoggerFactory.getLogger(ConfigurationParser.class);

    private final String configPath;

    public ConfigurationParser(String configPath) {
        this.configPath = configPath;
    }

    public List<Jingle> getJingles() {
        try(var lines = Files.lines(Path.of(configPath))) {
            return lines.filter(s -> !s.startsWith("#")).map(this::parseLine).toList();
        } catch (IOException e) {
            logger.error("Failed to load file: {}", configPath, e);
            return Collections.emptyList();
        }
    }

    private Jingle parseLine(String line) {
        var params = line.split(" ");

        if (params.length < 1) {
            throw new IllegalArgumentException(String.format("Could not parse line %s", line));
        }

        ZonedDateTime zdt;
        try {
            zdt = ZonedDateTime.parse(params[0]);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException(String.format("Could not parse date %s", params[0]), e);
        }

        var filepath = params[1];

        File file = new File(filepath);
        if (!file.exists() || !file.isFile()) {
            throw new IllegalArgumentException(String.format("File %s does not exists", params[1]));
        }

        logger.info("Parsed {} at {}", filepath, zdt);

        return new Jingle(filepath, zdt.toInstant());
    }
}
