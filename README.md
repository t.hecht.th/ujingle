# Ujingle

Open source Jingle scheduler which pauses spotify app at specific times and plays the configured jingle.

Building
--------

You need JDK 18 (or newer), Gradle 8.0 (or newer) and git installed.

    java -version
    gradle -version
    git --version

First clone the Utally repository:

    git clone https://gitlab.com/t.hecht.th/ujingle.git
    cd ujingle

To build Utally run:

    gradle build copyLicense
    gradle dists

This will build the artifact, documentation, the zip- and the tar-ball. You will find the distributions in `build/distributions`.

Installation
------------

### Prerequisites

Install the spotify mac app: https://www.spotify.com/de/download/mac/

Install the spotify command line interface (https://github.com/hnarayanan/shpotify):

    brew install shpotify

Install Java 18 or newer: https://openjdk.org/

### Configuration

Create a text document with any name, e.g. `ujingle.config`.

The configuration file contains the scheduling of the jingles in the following format:

    <ISO-8601-timestamp> <Path-to-mp3>

Example configuration with 2 jingles:

    2023-06-17T16:15:00+02:00 /Users/tohe/Music/move_your_ass.mp3
    2023-06-17T16:20:00+02:00 /Users/tohe/Music/hammertime.mp3

### Starting Ujingle

Run unjingle with the following command:

    <ujingle-directory>/bin/ujinglestart.sh <ujingle-onfig-directory>/ujingle.config